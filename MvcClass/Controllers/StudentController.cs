﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcClass.Models;

namespace MvcClass.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
          ViewBag.Message = "Awesome Students Page that's so easy to make!";

          return View(GetStudents());
        }

        private IEnumerable<Student> GetStudents()
        {
          var stud1 = new Student { StudentID = 1, FirstName = "Dillion", LastName = "Gitano" };
          var stud2 = new Student { StudentID = 2, FirstName = "Andrew", LastName = "White" };
          var studette1 = new Student { StudentID = 3, FirstName = "Barbara", LastName = "Karpf" };

          var studentList = new List<Student>();

          studentList.Add(stud1);
          studentList.Add(stud2);
          studentList.Add(studette1);
          return studentList.AsEnumerable();
        }
    }
}